# frozen_string_literal: true

# == Schema Information
#
# Table name: soutcomes
#
#  id          :bigint           not null, primary key
#  name        :string
#  status      :integer
#  target_date :date
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  smethod_id  :bigint           not null
#
# Indexes
#
#  index_soutcomes_on_smethod_id  (smethod_id)
#
# Foreign Keys
#
#  fk_rails_...  (smethod_id => smethods.id)
#
class Soutcome < ApplicationRecord
  belongs_to :smethod
end
