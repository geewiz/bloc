# frozen_string_literal: true

# == Schema Information
#
# Table name: areas
#
#  id           :bigint           not null, primary key
#  name         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  area_type_id :bigint           not null
#  user_id      :bigint           not null
#
# Indexes
#
#  index_areas_on_area_type_id  (area_type_id)
#  index_areas_on_user_id       (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (area_type_id => area_types.id)
#  fk_rails_...  (user_id => users.id)
#
class Area < ApplicationRecord
  belongs_to :area_type
  belongs_to :user
end
