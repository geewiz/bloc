# frozen_string_literal: true

# == Schema Information
#
# Table name: area_types
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class AreaType < ApplicationRecord
  has_many :areas, dependent: :destroy
end
