FROM ruby:2.6.5

RUN curl https://deb.nodesource.com/setup_12.x | bash && \
  curl https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -a && \
  echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
  apt-get update -qq && apt-get install -y \
    build-essential postgresql-client yarn \
    libpq-dev nodejs libqt4-dev libqtwebkit-dev

ENV APP_PATH=/app
RUN mkdir $APP_PATH
WORKDIR $APP_PATH

RUN gem install bundler
ADD Gemfile $APP_PATH/Gemfile
ADD Gemfile.lock $APP_PATH/Gemfile.lock
RUN bundle install

ADD package.json $APP_PATH/package.json
RUN yarn install

ADD . $APP_PATH
