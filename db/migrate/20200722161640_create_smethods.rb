class CreateSmethods < ActiveRecord::Migration[6.0]
  def change
    create_table :smethods do |t|
      t.string :name
      t.references :area, null: false, foreign_key: true
      t.integer :priority
      t.integer :status

      t.timestamps
    end
  end
end
