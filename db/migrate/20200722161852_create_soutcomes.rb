class CreateSoutcomes < ActiveRecord::Migration[6.0]
  def change
    create_table :soutcomes do |t|
      t.string :name
      t.references :smethod, null: false, foreign_key: true
      t.integer :status
      t.date :target_date

      t.timestamps
    end
  end
end
