class CreateActions < ActiveRecord::Migration[6.0]
  def change
    create_table :actions do |t|
      t.string :name
      t.references :project, null: true, foreign_key: true
      t.references :soutcome, null: true, foreign_key: true
      t.date :do_date
      t.datetime :deadline
      t.text :description
      t.integer :status
      t.integer :duration
      t.datetime :done_date
      t.references :depends_on, null: true,
                                foreign_key: { to_table: :actions },
                                index: true
      t.timestamps
    end
  end
end
