class CreateProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.references :soutcome, null: true, foreign_key: true
      t.integer :status
      t.date :deadline
      t.text :description

      t.timestamps
    end
  end
end
