class AddUserToAction < ActiveRecord::Migration[6.0]
  def change
    add_reference :actions, :user, null: true, foreign_key: true
  end
end
