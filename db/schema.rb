# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_11_154918) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "actions", force: :cascade do |t|
    t.string "name"
    t.bigint "project_id"
    t.bigint "soutcome_id"
    t.date "do_date"
    t.datetime "deadline"
    t.text "description"
    t.integer "status"
    t.integer "duration"
    t.datetime "done_date"
    t.bigint "depends_on_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id"
    t.index ["depends_on_id"], name: "index_actions_on_depends_on_id"
    t.index ["project_id"], name: "index_actions_on_project_id"
    t.index ["soutcome_id"], name: "index_actions_on_soutcome_id"
    t.index ["user_id"], name: "index_actions_on_user_id"
  end

  create_table "area_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "areas", force: :cascade do |t|
    t.string "name"
    t.bigint "area_type_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id", null: false
    t.index ["area_type_id"], name: "index_areas_on_area_type_id"
    t.index ["user_id"], name: "index_areas_on_user_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string "name"
    t.bigint "soutcome_id"
    t.integer "status"
    t.date "deadline"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id", null: false
    t.index ["soutcome_id"], name: "index_projects_on_soutcome_id"
    t.index ["user_id"], name: "index_projects_on_user_id"
  end

  create_table "smethods", force: :cascade do |t|
    t.string "name"
    t.bigint "area_id", null: false
    t.integer "priority"
    t.integer "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["area_id"], name: "index_smethods_on_area_id"
  end

  create_table "soutcomes", force: :cascade do |t|
    t.string "name"
    t.bigint "smethod_id", null: false
    t.integer "status"
    t.date "target_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["smethod_id"], name: "index_soutcomes_on_smethod_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "actions", "actions", column: "depends_on_id"
  add_foreign_key "actions", "projects"
  add_foreign_key "actions", "soutcomes"
  add_foreign_key "actions", "users"
  add_foreign_key "areas", "area_types"
  add_foreign_key "areas", "users"
  add_foreign_key "projects", "soutcomes"
  add_foreign_key "projects", "users"
  add_foreign_key "smethods", "areas"
  add_foreign_key "soutcomes", "smethods"
end
