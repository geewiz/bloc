# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  resources :users

  root to: "home#index"

  get "home/index"
end
