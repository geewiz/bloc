# frozen_string_literal: true

# == Schema Information
#
# Table name: smethods
#
#  id         :bigint           not null, primary key
#  name       :string
#  priority   :integer
#  status     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  area_id    :bigint           not null
#
# Indexes
#
#  index_smethods_on_area_id  (area_id)
#
# Foreign Keys
#
#  fk_rails_...  (area_id => areas.id)
#
require "rails_helper"

RSpec.describe Smethod, type: :model do
  it { is_expected.to belong_to :area }
  it { is_expected.to have_many :soutcomes }
end
