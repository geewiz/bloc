# frozen_string_literal: true

# == Schema Information
#
# Table name: projects
#
#  id          :bigint           not null, primary key
#  deadline    :date
#  description :text
#  name        :string
#  status      :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  soutcome_id :bigint
#  user_id     :bigint           not null
#
# Indexes
#
#  index_projects_on_soutcome_id  (soutcome_id)
#  index_projects_on_user_id      (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (soutcome_id => soutcomes.id)
#  fk_rails_...  (user_id => users.id)
#
require "rails_helper"

RSpec.describe Project, type: :model do
  it { is_expected.to belong_to :user }
  it { is_expected.to belong_to :soutcome }
end
