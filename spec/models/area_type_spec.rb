# frozen_string_literal: true

# == Schema Information
#
# Table name: area_types
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require "rails_helper"

RSpec.describe AreaType, type: :model do
  it { is_expected.to have_many :areas }
end
