# frozen_string_literal: true

RSpec.describe "the signin process", type: :feature do
  it "signs me in" do
    create(:user, email: "user@example.com", password: "password")

    visit "/users/sign_in"
    within("#new_user") do
      fill_in "Email", with: "user@example.com"
      fill_in "Password", with: "password"
    end
    click_button "Log in"

    expect(page).to have_content "successfully"
  end
end
