# frozen_string_literal: true

RSpec.describe "the registration process", type: :feature do
  it "lets me register" do
    visit "/users/sign_up"
    within("#new_user") do
      fill_in "Email", with: "user@example.com"
      fill_in "Password", with: "password"
      fill_in "Password confirmation", with: "password"
    end
    click_button "Sign up"

    user = User.first
    expect(user.email).to eq "user@example.com"
  end
end
