# frozen_string_literal: true

# == Schema Information
#
# Table name: actions
#
#  id            :bigint           not null, primary key
#  deadline      :datetime
#  description   :text
#  do_date       :date
#  done_date     :datetime
#  duration      :integer
#  name          :string
#  status        :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  depends_on_id :bigint
#  project_id    :bigint
#  soutcome_id   :bigint
#  user_id       :bigint
#
# Indexes
#
#  index_actions_on_depends_on_id  (depends_on_id)
#  index_actions_on_project_id     (project_id)
#  index_actions_on_soutcome_id    (soutcome_id)
#  index_actions_on_user_id        (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (depends_on_id => actions.id)
#  fk_rails_...  (project_id => projects.id)
#  fk_rails_...  (soutcome_id => soutcomes.id)
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :action do
    name { "MyString" }
    project { nil }
    soutcome { nil }
    do_date { "2020-07-22" }
    deadline { "2020-07-22 16:36:54" }
    description { "MyText" }
    status { 1 }
    duration { 1 }
    done_date { "2020-07-22 16:36:54" }
    depends_on { nil }
  end
end
