# BLOC -- Business & Life Operations Centre

An open-source Rails application for my dream productivity setup.

Inspired by August Bradley's ["Life Operating System"](https://www.youtube.com/user/augustbradley/), this application is a project and task management application that connects the "What" with the "Why". In other words, it doesn't just show you _what_ you are supposed to do but also the _reasons and motivations_ why you planned this work in the first place.

BLOC integrates several methodologies to a holistic system:

- Marc Benioff's V2MOM
- Google's OKR
- David Allen's GTD
- Simon Sinek's "Start with Why"

By connecting guiding priciples as well as business and life goals with concrete tasks, you can always make sure you have work lined up that actually gets you nearer to these goals. And, looking in the opposite direction, you can always see why a specific project or task is important.

## Development setup

Build and launch containers, initialise database (ideally run in a dedicated terminal):

```shell
docker-compose build
docker-compose up
docker-compose exec app bundle exec rails db:prepare
```
